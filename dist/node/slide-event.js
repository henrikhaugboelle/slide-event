'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _debounce = require('debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _eventListener = require('event-listener');

var _eventListener2 = _interopRequireDefault(_eventListener);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SlideEvent = function () {
  function SlideEvent() {
    _classCallCheck(this, SlideEvent);

    this.listeners = [];
  }

  _createClass(SlideEvent, [{
    key: 'addDesktopListener',
    value: function addDesktopListener(listener) {
      var previousWeight = null;
      var previousDirection = null;
      var isRising = false;

      var dispatchEvent = (0, _debounce2.default)(function (direction) {
        listener({ direction: direction });

        isRising = false;
        previousWeight = null;
        previousDirection = null;
      }, 200, true);

      var handleEvent = function handleEvent(event) {
        var delta = event.wheelDelta || event.detail;
        var weight = Math.abs(delta);
        var direction = event.wheelDelta > 0 || event.detail < 0 ? -1 : 1;
        var wasRising = isRising;

        isRising = previousWeight < weight;

        if (isRising !== wasRising) {
          dispatchEvent(direction);
        } else {
          previousWeight = weight;
          previousDirection = direction;
        }
      };

      this.listeners.push((0, _eventListener2.default)(document, 'mousewheel', handleEvent));
      this.listeners.push((0, _eventListener2.default)(document, 'DOMMouseScroll', handleEvent));
    }
  }, {
    key: 'addMobileListener',
    value: function addMobileListener(listener) {
      var xStart = null;
      var xStop = null;

      var yStart = null;
      var yStop = null;

      var dispatchEvent = (0, _debounce2.default)(function () {
        var diffX = xStart - xStop;
        var diffY = yStart - yStop;

        if (Math.abs(diffX) < 100 && Math.abs(diffY) > 50) {
          var direction = diffY < 0 ? -1 : 1;
          listener({ direction: direction });
        }

        xStart = null;
        xStop = null;
        yStart = null;
        yStop = null;
      }, 200, false);

      var handleEvent = function handleEvent(event) {
        xStart = xStart || event.touches[0].clientX;
        xStop = event.touches[0].clientX;

        yStart = yStart || event.touches[0].clientY;
        yStop = event.touches[0].clientY;

        dispatchEvent();
      };

      this.listeners.push((0, _eventListener2.default)(document, 'touchmove', handleEvent));
    }
  }, {
    key: 'addListener',
    value: function addListener(listener) {
      this.addDesktopListener(listener);
      this.addMobileListener(listener);
    }
  }, {
    key: 'removeListeners',
    value: function removeListeners() {
      this.listeners.forEach(function (listener) {
        listener.remove();
      });
    }
  }]);

  return SlideEvent;
}();

var instance = new SlideEvent();

window.SlideEvent = instance;

exports.default = instance;