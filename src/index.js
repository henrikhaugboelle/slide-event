import debounce from 'debounce'
import addEventListener from 'event-listener'

class SlideEvent {
  constructor() {
    this.listeners = []
  }

  addDesktopListener(listener) {
    let previousWeight = null
    let previousDirection = null
    let isRising = false

    const dispatchEvent = debounce(direction => {
      listener({ direction })

      isRising = false
      previousWeight = null
      previousDirection = null
    }, 200, true)

    const handleEvent = event => {
      const delta = event.wheelDelta || event.detail
      const weight = Math.abs(delta)
      const direction = event.wheelDelta > 0 || event.detail < 0 ? -1 : 1
      const wasRising = isRising

      isRising = previousWeight < weight

      if (isRising !== wasRising) {
        dispatchEvent(direction)
      } else {
        previousWeight = weight
        previousDirection = direction
      }
    }

    this.listeners.push(addEventListener(document, 'mousewheel', handleEvent))
    this.listeners.push(addEventListener(document, 'DOMMouseScroll', handleEvent))
  }

  addMobileListener(listener) {
    let xStart = null
    let xStop = null
    
    let yStart = null
    let yStop = null

    const dispatchEvent = debounce(() => {
      var diffX = xStart - xStop
      var diffY = yStart - yStop

      if (Math.abs(diffX) < 100 && Math.abs(diffY) > 50) {
        const direction = diffY < 0 ? -1 : 1
        listener({ direction })
      }

      xStart = null
      xStop = null
      yStart = null
      yStop = null
    }, 200, false)

    const handleEvent = (event) => {
      xStart = xStart || event.touches[0].clientX
      xStop = event.touches[0].clientX

      yStart = yStart || event.touches[0].clientY
      yStop = event.touches[0].clientY

      dispatchEvent()
    }

    this.listeners.push(addEventListener(document, 'touchmove', handleEvent))
  }

  addListener(listener) {
    this.addDesktopListener(listener)
    this.addMobileListener(listener)
  }

  removeListeners() {
    this.listeners.forEach(listener => {
      listener.remove()
    })
  }
}

const instance = new SlideEvent

window.SlideEvent = instance

export default instance