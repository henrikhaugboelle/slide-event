(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = Date.now || now

function now() {
    return new Date().getTime()
}

},{}],2:[function(require,module,exports){

/**
 * Module dependencies.
 */

var now = require('date-now');

/**
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing.
 *
 * @source underscore.js
 * @see http://unscriptable.com/2009/03/20/debouncing-javascript-methods/
 * @param {Function} function to wrap
 * @param {Number} timeout in ms (`100`)
 * @param {Boolean} whether to execute at the beginning (`false`)
 * @api public
 */

module.exports = function debounce(func, wait, immediate){
  var timeout, args, context, timestamp, result;
  if (null == wait) wait = 100;

  function later() {
    var last = now() - timestamp;

    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last);
    } else {
      timeout = null;
      if (!immediate) {
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      }
    }
  };

  return function debounced() {
    context = this;
    args = arguments;
    timestamp = now();
    var callNow = immediate && !timeout;
    if (!timeout) timeout = setTimeout(later, wait);
    if (callNow) {
      result = func.apply(context, args);
      context = args = null;
    }

    return result;
  };
};

},{"date-now":1}],3:[function(require,module,exports){
'use strict'

function listen(target, eventType, callback) {
  if (target == null) {
    throw new TypeError('target must be provided')
  }
  if (Object.prototype.toString.call(eventType) !== '[object String]') {
    throw new TypeError('eventType must be a string')
  }

  var eventTypes = eventType.split(' ').filter(Boolean)
  if (eventTypes.length === 0) {
    throw new Error('eventType must not be blank')
  }

  if (target.addEventListener) {
    eventTypes.forEach(function(eventType) {
      target.addEventListener(eventType, callback, false)
    })
    return {
      remove: function() {
        eventTypes.forEach(function(eventType) {
          target.removeEventListener(eventType, callback, false)
        })
      }
    }
  }
  else if (target.attachEvent) {
    eventTypes.forEach(function(eventType) {
      target.attachEvent('on' + eventType, callback)
    })
    return {
      remove: function() {
        eventTypes.forEach(function(eventType) {
          target.detachEvent('on' + eventType, callback)
        })
      }
    }
  }
  else {
    throw new TypeError('target must have addEventListener or attachEvent')
  }
}

module.exports = listen

},{}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _debounce = require('debounce');

var _debounce2 = _interopRequireDefault(_debounce);

var _eventListener = require('event-listener');

var _eventListener2 = _interopRequireDefault(_eventListener);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SlideEvent = function () {
  function SlideEvent() {
    _classCallCheck(this, SlideEvent);

    this.listeners = [];
  }

  _createClass(SlideEvent, [{
    key: 'addDesktopListener',
    value: function addDesktopListener(listener) {
      var previousWeight = null;
      var previousDirection = null;
      var isRising = false;

      var dispatchEvent = (0, _debounce2.default)(function (direction) {
        listener({ direction: direction });

        isRising = false;
        previousWeight = null;
        previousDirection = null;
      }, 200, true);

      var handleEvent = function handleEvent(event) {
        var delta = event.wheelDelta || event.detail;
        var weight = Math.abs(delta);
        var direction = event.wheelDelta > 0 || event.detail < 0 ? -1 : 1;
        var wasRising = isRising;

        isRising = previousWeight < weight;

        if (isRising !== wasRising) {
          dispatchEvent(direction);
        } else {
          previousWeight = weight;
          previousDirection = direction;
        }
      };

      this.listeners.push((0, _eventListener2.default)(document, 'mousewheel', handleEvent));
      this.listeners.push((0, _eventListener2.default)(document, 'DOMMouseScroll', handleEvent));
    }
  }, {
    key: 'addMobileListener',
    value: function addMobileListener(listener) {
      var xStart = null;
      var xStop = null;

      var yStart = null;
      var yStop = null;

      var dispatchEvent = (0, _debounce2.default)(function () {
        var diffX = xStart - xStop;
        var diffY = yStart - yStop;

        if (Math.abs(diffX) < 100 && Math.abs(diffY) > 50) {
          var direction = diffY < 0 ? -1 : 1;
          listener({ direction: direction });
        }

        xStart = null;
        xStop = null;
        yStart = null;
        yStop = null;
      }, 200, false);

      var handleEvent = function handleEvent(event) {
        xStart = xStart || event.touches[0].clientX;
        xStop = event.touches[0].clientX;

        yStart = yStart || event.touches[0].clientY;
        yStop = event.touches[0].clientY;

        dispatchEvent();
      };

      this.listeners.push((0, _eventListener2.default)(document, 'touchmove', handleEvent));
    }
  }, {
    key: 'addListener',
    value: function addListener(listener) {
      this.addDesktopListener(listener);
      this.addMobileListener(listener);
    }
  }, {
    key: 'removeListeners',
    value: function removeListeners() {
      this.listeners.forEach(function (listener) {
        listener.remove();
      });
    }
  }]);

  return SlideEvent;
}();

var instance = new SlideEvent();

window.SlideEvent = instance;

exports.default = instance;
},{"debounce":2,"event-listener":3}]},{},[4])