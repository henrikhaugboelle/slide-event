# SlideEvent

Simple library for listening on vertical "slide events" initiated trackpad, mouse, mobile touch, etc.

Not to be confused with general gesture libraries handling various swipe events as known from e.g. [HammerJS](http://hammerjs.github.io/) and [QuoJS](http://quojs.tapquo.com/).

Inspired by the slide feature from [fullPage.js](http://www.alvarotrigo.com/fullPage/).

Minified library size: **3KB**.

### NPM installation

    npm install slide-event

### Browser installation

Use development or minified version found in the `dist/browser` folder of this repository.

### Usage
    
    // Import (node usage)
    import SlideEvent from 'slide-event'

    // Add listener
    SlideEvent.addListener(event => {
      console.log(event.direction) // -1 if sliding up, 1 if sliding down
    })

    // Remove all listeners
    SlideEvent.removeListeners()

### Demo

A demo is found the `demo` folder of this repository. Install http-server globally `npm install -g http-server` and run it from the root of this repository `http-server .`, you can then access the `/demo/` folder and try out the demo ... Or you can just click below ;-)

[Try demo](http://henrikhaugboelle.gitlab.io/slide-event/index.html)

### Event support

- Trackpad scrolling
- Mouse scrolling
- Mobile screen scrolling

### Browser support

SlideEvent is tested in latest versions of Chrome, Firefox, and Safari on macOS. Tested in Chrome on Android. The compiled browser version supports legacy dom event binding and uses regular old fashioned JavaScript, so it should work in all major browsers (including legacy versions.)

Tested: November 2016

### Custom build

Want to build your own version? Sure! Just follow these instructions:

    npm install
    npm run build

And voilá, you will have your own custom builds in the `dist` folder. Easy, eh?

### Want to contribute or anything interesting to say?

Please do so, hit me up at [henrikhaugboelle@gmail.com](henrikhaugboelle@gmail.com) :-)