const Gulp = require('gulp')
const Rename = require('gulp-rename')
const Browserify = require('gulp-browserify')
const Babel = require('gulp-babel')
const Uglify = require('gulp-uglify')

Gulp.task('build', ['node', 'browser:development', 'browser:minified'])

Gulp.task('node', () => {
  return Gulp.src('src/index.js')
    .pipe(Babel({ presets: ['es2015'] }))
    .pipe(Rename('slide-event.js'))
    .pipe(Gulp.dest('dist/node'))
})

Gulp.task('browser:development', () => {
  return Gulp.src('src/index.js')
    .pipe(Babel({ presets: ['es2015'] }))
    .pipe(Browserify({}))
    .pipe(Rename('slide-event.js'))
    .pipe(Gulp.dest('dist/browser'))
})

Gulp.task('browser:minified', () => {
  return Gulp.src('src/index.js')
    .pipe(Babel({ presets: ['es2015'] }))
    .pipe(Browserify({}))
    .pipe(Uglify({}))
    .pipe(Rename('slide-event.min.js'))
    .pipe(Gulp.dest('dist/browser'))
})